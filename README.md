<h1 style="text-align: center !important;">icee-config-rs</h1>

<br />

<div align="center">
  <a href="https://crates.io/crates/icee-config-rs">
    <img src="https://img.shields.io/crates/v/icee-config-rs.svg?style=flat-square"
    alt="Crates.io" />
  </a>
  <a href="https://docs.rs/icee-config-rs">
    <img src="https://img.shields.io/badge/docs-latest-blue.svg?style=flat-square"
      alt="docs.rs" />
  </a>
</div>

<br />

Wrapper for config.rs to easily load configuration folders with env based overrides

- Supported formats ["toml", "ini", "json", "hjson", "yaml", "yml"]
- Overrides with folder specified in APP_ENV (default 'dev')
- Overrides with environment variables

