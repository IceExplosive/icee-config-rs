use std::env::var;
use std::fs::read_dir;

use config::{Config, Environment, File};
use log::{debug, info};
use serde::Deserialize;

/// Configuration loader options
pub struct Options {
    /// Root config folder (defaults to 'config')
    pub root_path: String,
    /// If not set, use APP_ENV environment value or defaults to 'dev'
    pub env: Option<String>,
    /// Loads env variables (defaults to true)
    pub read_envs: bool,
    /// Environment var separator
    pub env_separator: Option<String>,
}

/// Loads a configuration
///
/// Requires predefined struct with all config options
///
/// ```
/// use serde::Deserialize;
/// use icee_config_rs::{load, Options};
///
/// #[derive(Deserialize)]
/// struct C {
///     pub value: Option<String>,
/// }
///
/// let parsed = load::<C>(Options::new());
/// # assert_eq!(parsed.value, None);
/// ```
pub fn load<'a, T: Deserialize<'a>>(options: Options) -> T {
    info!("Parsing configuration...");
    let mut config = Config::new();

    load_dir(&mut config, &options.root_path);

    let env: String = match options.env {
        Some(v) => v,
        None => var("APP_ENV").unwrap_or_else(|_| "dev".into())
    };

    load_dir(&mut config, &format!("{}/{}", &options.root_path, env));

    let mut env_opt = Environment::new();
    if let Some(separator) = options.env_separator {
        env_opt = env_opt.separator(&separator);
    }

    config.merge(env_opt).unwrap();

    config.try_into().expect("loading configuration")
}

/// Options for loading configuration files
impl Options {
    /// Prepare default options
    ///
    /// ```
    /// use icee_config_rs::Options;
    ///
    /// let opt = Options::new();
    /// # assert_eq!(opt.env, None);
    /// # assert_eq!(opt.root_path, "config");
    /// # assert_eq!(opt.read_envs, true);
    /// ```
    pub fn new() -> Self {
        Options {
            root_path: "config".into(),
            env: None,
            read_envs: true,
            env_separator: Some("__".into()),
        }
    }

    /// Default options with different root path
    ///
    /// ```
    /// use icee_config_rs::Options;
    ///
    /// let opt = Options::with_root_path("conf");
    /// # assert_eq!(opt.env, None);
    /// # assert_eq!(opt.root_path, "conf");
    /// # assert_eq!(opt.read_envs, true);
    /// ```
    pub fn with_root_path(root_path: &str) -> Self {
        Options {
            root_path: root_path.into(),
            env: None,
            read_envs: true,
            env_separator: Some("__".into()),
        }
    }

    /// Default options with different env settings
    ///
    /// ```
    /// use icee_config_rs::Options;
    ///
    /// let opt = Options::with_app_env("test");
    /// # assert_eq!(opt.env, Some("test".into()));
    /// # assert_eq!(opt.root_path, "config");
    /// # assert_eq!(opt.read_envs, true);
    /// ```
    pub fn with_app_env(env: &str) -> Self {
        Options {
            root_path: "config".into(),
            env: Some(env.into()),
            read_envs: true,
            env_separator: Some("__".into()),
        }
    }
}

impl Default for Options {
    fn default() -> Self {
        Self::new()
    }
}

fn load_dir(config: &mut Config, dirname: &str) {
    let supported = ["toml", "ini", "json", "hjson", "yaml", "yml"];
    debug!("Walking {} config directory...", dirname);

    if let Ok(dir) = read_dir(dirname) {
        for path in dir {
            let path = path.unwrap().path();
            let filename = path.to_str().unwrap();

            if let Some(ext) = filename.split('.').last() {
                if supported.contains(&ext) {
                    config
                        .merge(File::with_name(filename))
                        .unwrap_or_else(|_| panic!("invalid config file {}", filename));
                }
            }
        }
    }
}
