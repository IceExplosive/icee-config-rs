use icee_config_rs::{Options, load};
use serde::Deserialize;

#[derive(Deserialize)]
struct Config {
    var: usize,
    base: usize,
}

#[test]
fn test_config() {
    let opt = Options::with_root_path("tests/config");

    let c = load::<Config>(opt);
    assert_eq!(c.var, 6);
    assert_eq!(c.base, 15);

    let c = load::<Config>(Options {
        root_path: "tests/config".into(),
        env: Some("test".into()),
        read_envs: false,
        env_separator: None,
    });
    assert_eq!(c.var, 8);
    assert_eq!(c.base, 15);

    let c = load::<Config>(Options {
        root_path: "tests/config".into(),
        env: Some("random".into()),
        read_envs: false,
        env_separator: None,
    });
    assert_eq!(c.var, 1);
    assert_eq!(c.base, 15);
}
